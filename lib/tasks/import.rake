#encoding: utf-8
require 'csv'

namespace :import do

  desc 'Importar inventario'
  task :inventario, [:file] => :environment do |t, args|
    puts "Processando inventário..."
    
    CSV.foreach('./docs/inventario_2013.csv',:col_sep => '|', encoding: "ISO8859-1") do |col|
      classe = col[0].to_s.strip
      tombamento = col[1].strip
      descricao = col[2].strip
      localizacao = col[3].strip
      valor_aquisicao = col[4].strip.to_f
      puts col[5]
      dt_aquisicao = Date.parse(col[5].to_s.strip)
      ec = col[6].to_s.strip
      pub = col[7].to_s.strip
      evf = col[8].to_s.strip
      fr = col[9].strip.gsub('%','').to_f
      valor_mercado = col[10].strip.to_f
      vbr = col[11].strip.to_f
      vr = col[12].strip.to_f
      nv = col[13].strip.to_f

      item = Item.new

      item.classe = classe
      item.tombamento = tombamento
      item.descricao = descricao
      item.localizacao = localizacao
      item.valor_aquisicao = valor_aquisicao
      item.dt_aquisicao = dt_aquisicao
      item.ec = ec
      item.pub = pub
      item.evf = evf
      item.fr = fr
      item.valor_mercado = valor_mercado
      item.vbr = vbr
      item.vr = vr
      item.nv = nv

      item.save
    end    
  end

end
