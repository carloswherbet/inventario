class CreateItens < ActiveRecord::Migration
  def change
    create_table :itens do |t|
      t.string :classe
      t.string :tombamento
      t.string :descricao
      t.string :localizacao
      t.decimal :valor_aquisicao, :precision => 8, :scale => 2
      t.date :dt_aquisicao
      t.string :ec
      t.string :pub
      t.string :evf
      t.decimal :fr, :precision => 8, :scale => 2
      t.decimal :valor_mercado, :precision => 8, :scale => 2
      t.decimal :vbr, :precision => 8, :scale => 2
      t.decimal :vr, :precision => 8, :scale => 2
      t.decimal :nv, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
