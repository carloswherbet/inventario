json.array!(@itens) do |item|
  json.extract! item, :id, :classe, :tombamento, :descricao, :localizacao, :valor_aquisicao, :dt_aquisicao, :ec, :pub, :evf, :fr, :valor_mercado, :vbr, :vr, :nv
  json.url item_url(item, format: :json)
end
