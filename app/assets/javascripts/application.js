// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


		var options = {
	      selectOptionLabel: '',
	      enableCookies: false
	    };
	  	$('#table-filtros').tableFilter(options);
var javascripts = function() {


	$(document).ready(function() {
		$('#table-filtros').tableFilter();
	});


}

var do_on_load = function(){
	javascripts();
}

$(document).ready(do_on_load)
$(window).bind('page:load', do_on_load)
