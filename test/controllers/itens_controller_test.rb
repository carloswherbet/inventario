require 'test_helper'

class ItensControllerTest < ActionController::TestCase
  setup do
    @item = itens(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:itens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post :create, item: { classe: @item.classe, descricao: @item.descricao, dt_aquisicao: @item.dt_aquisicao, ec: @item.ec, evf: @item.evf, fr: @item.fr, localizacao: @item.localizacao, nv: @item.nv, pub: @item.pub, tombamento: @item.tombamento, valor_aquisicao: @item.valor_aquisicao, valor_mercado: @item.valor_mercado, vbr: @item.vbr, vr: @item.vr }
    end

    assert_redirected_to item_path(assigns(:item))
  end

  test "should show item" do
    get :show, id: @item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @item
    assert_response :success
  end

  test "should update item" do
    patch :update, id: @item, item: { classe: @item.classe, descricao: @item.descricao, dt_aquisicao: @item.dt_aquisicao, ec: @item.ec, evf: @item.evf, fr: @item.fr, localizacao: @item.localizacao, nv: @item.nv, pub: @item.pub, tombamento: @item.tombamento, valor_aquisicao: @item.valor_aquisicao, valor_mercado: @item.valor_mercado, vbr: @item.vbr, vr: @item.vr }
    assert_redirected_to item_path(assigns(:item))
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete :destroy, id: @item
    end

    assert_redirected_to itens_path
  end
end
